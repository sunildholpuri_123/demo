package com.example.demo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("flightName")
    @Expose
    private String flightName;
    @SerializedName("airlineCode")
    @Expose
    private String airlineCode;
    @SerializedName("scheduleDate")
    @Expose
    private String scheduleDate;
    @SerializedName("prefixIATA")
    @Expose
    private String prefixIATA;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("estimatedDate")
    @Expose
    private String estimatedDate;
    @SerializedName("via1")
    @Expose
    private String via1;
    @SerializedName("via2")
    @Expose
    private String via2;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("originmap")
    @Expose
    private String originmap;
    @SerializedName("destinationmap")
    @Expose
    private String destinationmap;
    @SerializedName("scheduleTime")
    @Expose
    private String scheduleTime;
    @SerializedName("estimatedTime")
    @Expose
    private String estimatedTime;
    @SerializedName("airlineName")
    @Expose
    private String airlineName;
    @SerializedName("gates")
    @Expose
    private String gates;
    @SerializedName("terminal")
    @Expose
    private String terminal;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("nature")
    @Expose
    private String nature;
    @SerializedName("qualifier")
    @Expose
    private String qualifier;
    @SerializedName("scheduledArrivalDate")
    @Expose
    private String scheduledArrivalDate;
    @SerializedName("estimatedArrivalDate")
    @Expose
    private String estimatedArrivalDate;
    @SerializedName("scheduledArrivalTime")
    @Expose
    private String scheduledArrivalTime;
    @SerializedName("estimatedArrivalTime")
    @Expose
    private String estimatedArrivalTime;
    @SerializedName("flight_number_master")
    @Expose
    private String flight_number_master;
    @SerializedName("airline_code_master")
    @Expose
    private String airline_code_master;
    @SerializedName("airline_name_master")
    @Expose
    private String airline_name_master;
    @SerializedName("airline_code_icao_master")
    @Expose
    private String airline_code_icao_master;
    @SerializedName("carousel")
    @Expose
    private String carousel;
    @SerializedName("counters")
    @Expose
    private String counters;
    @SerializedName("airline_name_master_multilingual")
    @Expose
    private String airline_name_master_multilingual;
    @SerializedName("status_multilingual")
    @Expose
    private String status_multilingual;
    @SerializedName("originmap_multilingual")
    @Expose
    private String originmap_multilingual;
    @SerializedName("destinationmap_multilingual")
    @Expose
    private String destinationmap_multilingual;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getPrefixIATA() {
        return prefixIATA;
    }

    public void setPrefixIATA(String prefixIATA) {
        this.prefixIATA = prefixIATA;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEstimatedDate() {
        return estimatedDate;
    }

    public void setEstimatedDate(String estimatedDate) {
        this.estimatedDate = estimatedDate;
    }

    public String getVia1() {
        return via1;
    }

    public void setVia1(String via1) {
        this.via1 = via1;
    }

    public String getVia2() {
        return via2;
    }

    public void setVia2(String via2) {
        this.via2 = via2;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginmap() {
        return originmap;
    }

    public void setOriginmap(String originmap) {
        this.originmap = originmap;
    }

    public String getDestinationmap() {
        return destinationmap;
    }

    public void setDestinationmap(String destinationmap) {
        this.destinationmap = destinationmap;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getGates() {
        return gates;
    }

    public void setGates(String gates) {
        this.gates = gates;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getQualifier() {
        return qualifier;
    }

    public void setQualifier(String qualifier) {
        this.qualifier = qualifier;
    }

    public String getScheduledArrivalDate() {
        return scheduledArrivalDate;
    }

    public void setScheduledArrivalDate(String scheduledArrivalDate) {
        this.scheduledArrivalDate = scheduledArrivalDate;
    }

    public String getEstimatedArrivalDate() {
        return estimatedArrivalDate;
    }

    public void setEstimatedArrivalDate(String estimatedArrivalDate) {
        this.estimatedArrivalDate = estimatedArrivalDate;
    }

    public String getScheduledArrivalTime() {
        return scheduledArrivalTime;
    }

    public void setScheduledArrivalTime(String scheduledArrivalTime) {
        this.scheduledArrivalTime = scheduledArrivalTime;
    }

    public String getEstimatedArrivalTime() {
        return estimatedArrivalTime;
    }

    public void setEstimatedArrivalTime(String estimatedArrivalTime) {
        this.estimatedArrivalTime = estimatedArrivalTime;
    }

    public String getFlight_number_master() {
        return flight_number_master;
    }

    public void setFlight_number_master(String flight_number_master) {
        this.flight_number_master = flight_number_master;
    }

    public String getAirline_code_master() {
        return airline_code_master;
    }

    public void setAirline_code_master(String airline_code_master) {
        this.airline_code_master = airline_code_master;
    }

    public String getAirline_name_master() {
        return airline_name_master;
    }

    public void setAirline_name_master(String airline_name_master) {
        this.airline_name_master = airline_name_master;
    }

    public String getAirline_code_icao_master() {
        return airline_code_icao_master;
    }

    public void setAirline_code_icao_master(String airline_code_icao_master) {
        this.airline_code_icao_master = airline_code_icao_master;
    }

    public String getCarousel() {
        return carousel;
    }

    public void setCarousel(String carousel) {
        this.carousel = carousel;
    }

    public String getCounters() {
        return counters;
    }

    public void setCounters(String counters) {
        this.counters = counters;
    }

    public String getAirline_name_master_multilingual() {
        return airline_name_master_multilingual;
    }

    public void setAirline_name_master_multilingual(String airline_name_master_multilingual) {
        this.airline_name_master_multilingual = airline_name_master_multilingual;
    }

    public String getStatus_multilingual() {
        return status_multilingual;
    }

    public void setStatus_multilingual(String status_multilingual) {
        this.status_multilingual = status_multilingual;
    }

    public String getOriginmap_multilingual() {
        return originmap_multilingual;
    }

    public void setOriginmap_multilingual(String originmap_multilingual) {
        this.originmap_multilingual = originmap_multilingual;
    }

    public String getDestinationmap_multilingual() {
        return destinationmap_multilingual;
    }

    public void setDestinationmap_multilingual(String destinationmap_multilingual) {
        this.destinationmap_multilingual = destinationmap_multilingual;
    }

}
