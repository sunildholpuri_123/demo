package com.example.demo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.FlightViewHolder> {

    Context mCtx;
    List<Flight> flightList;

    public FlightAdapter(Context mCtx, List<Flight> flightList) {
        this.mCtx = mCtx;
        this.flightList = flightList;
    }

    @NonNull
    @Override
    public FlightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerview_layout, parent, false);
        return new FlightViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FlightViewHolder holder, int position) {
        Flight hero = flightList.get(position);


        holder.Binder(hero);
    }

    @Override
    public int getItemCount() {
        return flightList.size();
    }

    public String millsToDateFormat(long mills) {

        Date date = new Date(mills);
        DateFormat formatter = new SimpleDateFormat("HH:mm a");
        String dateFormatted = formatter.format(date);
        return dateFormatted; //note that it will give you the time in GMT+0
    }

    class FlightViewHolder extends RecyclerView.ViewHolder {

        TextView tvSource, tvDest, tvFlight, tvTime, tvDesk, tvTerminal, tvGate;

        public FlightViewHolder(View itemView) {
            super(itemView);

            tvSource = itemView.findViewById(R.id.tvSource);
            tvDest = itemView.findViewById(R.id.tvDest);
            tvFlight = itemView.findViewById(R.id.tvFlight);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvDesk = itemView.findViewById(R.id.tvDesk);
            tvTerminal = itemView.findViewById(R.id.tvTerminal);
            tvGate = itemView.findViewById(R.id.tvGate);

        }

        public void Binder(Flight flight) {
            tvSource.setText("From " + flight.getOriginmap() + " to");
            tvDest.setText(flight.getDestinationmap());
            tvFlight.setText(flight.getFlightName());
            String desk = "<b>" + flight.getAirline_code_icao_master() + "</b> ";
            tvDesk.setText("Check-in Desk " + Html.fromHtml(desk));
            String terminal = "<b>" + flight.getTerminal() + "</b> ";
            tvTerminal.setText("Terminal " + Html.fromHtml(terminal));
            tvTime.setText(millsToDateFormat(Long.parseLong(flight.getEstimatedTime())));
            String gate = "<b>" + flight.getGates() + "</b> ";

            tvGate.setText("Gate " + Html.fromHtml(gate));


        }
    }
}
