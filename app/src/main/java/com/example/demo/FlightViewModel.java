package com.example.demo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FlightViewModel extends ViewModel {

    //this is the data that we will fetch asynchronously
    private MutableLiveData<List<Flight>> heroList;

    //we will call this method to get the data
    public LiveData<List<Flight>> getHeroes() {
        //if the list is null
        if (heroList == null) {
            heroList = new MutableLiveData<>();
            //we will load it asynchronously from server in this method
            loadHeroes();
        }

        //finally we will return the list
        return heroList;
    }


    //This method is using Retrofit to get the JSON data from URL
    private void loadHeroes() {
        long twoHours = 7200000;
        long time = System.currentTimeMillis();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<JsonResponse> call = api.getFlight("cgk", (time - twoHours), (time + twoHours));


        call.enqueue(new Callback<JsonResponse>() {
            @Override
            public void onResponse(Call<JsonResponse> call, Response<JsonResponse> response) {

                //finally we are setting the list to our MutableLiveData
                heroList.setValue(response.body().getFlights());
            }

            @Override
            public void onFailure(Call<JsonResponse> call, Throwable t) {

            }
        });
    }
}
