package com.example.demo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    String BASE_URL = "http://development.wisefly.in/";

    @GET("flightsroute/getflightfeed")
    Call<JsonResponse> getFlight(@Query("appname") String appname, @Query("starttime") long starttime, @Query("endtime") long endtime);


}
